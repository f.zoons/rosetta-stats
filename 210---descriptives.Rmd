# Descriptives {#descriptives}

## Intro

The term 'descriptives' is typically used to refer to a set of measures summarizing a distribution, such as central tendency and spread measures.

### Example dataset

This example uses the Rosetta Stats example dataset "pp15" (see Chapter \@ref(datasets) for information about the datasets and Chapter \@ref(loading-data) for an explanation of how to load datasets).

### Variable(s)

From this dataset, this example uses variables `xtcUseDoseHigh`, `highDose_intention`, `highDose_attitude` (all three numeric variables) and `hasJob_bi` (a dichotomous factor, i.e. a categorical variable).

## Input: jamovi

```{r fig.cap='Opening the "Exploration" menu in jamovi.'}
knitr::include_graphics(here::here("img","210-descriptives-jamovi-dialog.png"));
```

## Input: R

### R: base

Use the following command:

```r
summary(
  dat[,
    c(
      "xtcUseDoseHigh",
      "highDose_intention",
      "highDose_attitude",
      "hasJob_bi"
    )
  ]
);
```

### R: Rosetta

Use the following command:

```r
rosetta::descr(
  dat,
  items = c(
    "xtcUseDoseHigh",
    "highDose_intention",
    "highDose_attitude",
    "hasJob_bi"
  ),
  histogram = TRUE,
  boxplot = TRUE
);
```

<!-- NOTE: this will also become possible for multiple variables in rosetta v0.3.2! -->

When ordering descriptives for a single variable (and in version 0.3.2 of the `rosetta` package, also for multiple variables), you can finetune which descriptives you get by passing `TRUE` or `FALSE` for arguments `mean`, `meanCI`, `median`, `mode`, `var`, `sd`, `se`, `min`, `max`, `q1`, `q3`, `IQR`, `skewness`, `kurtosis`, `dip`, `totalN`, `missingN`, and `validN`.

In addition, you can order histograms (or bar charts, for factors) and box plots (for numeric variables only) by passing `histogram=TRUE` and `boxplot=TRUE`, respectively.

If you omit the `items` argument, you get descriptives for all variables in the dataframe; and if you don't pass a dataframe, but a single variable, you will just get the descriptives for that variable.

## Input: SPSS

### SPSS: GUI

First activate the `pp15` dataset by clicking on it (see \@ref(software-basics-spss-active-dataset)).

```{r 210-descriptives-spss-input, fig.cap='Opening the "descriptives menu" in SPSS'}
knitr::include_graphics(here::here("img","210-descriptives-spss-dialog.png"));
```

Then select the variables of interest.

```{r 210-descriptives-spss-input-part2, fig.cap='Selection of variables of interest'}
knitr::include_graphics(here::here("img","210-descriptives-spss-dialog2.png"));
```


### SPSS: Syntax

Use the following command (this requires the `dat` dataset to be the active dataset, see \@ref(software-basics-spss-active-dataset)):

```
DESCRIPTIVES VARIABLES=xtcUseDoseHigh highDose_intention highDose_attitude hasJob_bi 
  /STATISTICS=MEAN STDDEV MIN MAX.
```

## Output: jamovi

```{r 210-descriptives-jamovi-output-chunk, fig.cap='The produced descriptives output in jamovi'}
knitr::include_graphics(here::here("img","210-descriptives-jamovi-output.png"));
```

## Output: R

### R: rosetta

```{r 210-descriptives-R-output-chunk, echo=FALSE, fig.width=6, fig.height=6}
data('pp15', package='rosetta');
rosetta::descr(
  pp15,
  items = c(
    "xtcUseDoseHigh",
    "highDose_intention",
    "highDose_attitude",
    "hasJob_bi"
  ),
  histogram = TRUE,
  boxplot = TRUE,
  headingLevel = 4
);
```

## Output: SPSS

```{r 210-descriptives-spss-output, echo=FALSE, fig.cap='The descriptives produced in SPSS.', fig.width=6, fig.height=6}
knitr::include_graphics(here::here("img","210-descriptives-spss-output.png"));
```

## Read more

If you would like more background on this topic, you can read more in these sources:

- Discovering Statistics with SPSS (closed access): 
- Learning Statistics with R [@navarro_learning_2018]: section XXXXXXXXXX, page XXXXXXXXXX (or [follow this link for the Bookdown version](https://bookdown.org/ekothe/navarro26/XXXXXXXXXX))

