# Frequencies {#frequencies}

## Intro

Frequency tables are normally used to inspect the distribution of categorical (dichotomous, nominal or ordinal) variables.

### Example dataset

This example uses the Rosetta Stats example dataset "pp15" (see Chapter \@ref(datasets) for information about the datasets and Chapter \@ref(loading-data) for an explanation of how to load datasets).

### Variable(s)

From this dataset, this example uses variable `currentEducation_cat`.

## jamovi

In the **analyses** tab there is a symbol with a barchart, called 'Exploration'. Go to this menu and select **Descriptives**. Drag the variables ypu want to describe to the variables window and check the little box **Frequency tables** to obtain a frequency table.  

## R

There are **many** packages that can be used to create a frequency table. We have only presented two examples. Other packages include (but are not limited to):

- [summarytools](https://cran.r-project.org/web/packages/summarytools/summarytools.pdf)
- [Deducer](https://cran.r-project.org/web/packages/Deducer/Deducer.pdf)
- [janitor](https://cran.r-project.org/web/packages/janitor/janitor.pdf)
- [questionr](https://cran.r-project.org/web/packages/questionr/questionr.pdf)
- [sjmisc](https://cran.r-project.org/web/packages/sjmisc/sjmisc.pdf)

  - If you read an SPSS dataset into R, consider using the "frq" command from the "sjmisc" package. It presents both values and value labels (similar to SPSS output).

**Note**: To use the following commands, it is necessary to install and load the packages first (see section \@ref(software-basics-r-packages)).  The example dataset is stored under the name `dat` (see section \@ref(loading-data)).

### rosetta package

<!--maybe put these instructions at the beginning of every R section instead? see above-->

Use the following command (this requires the `rosetta` package to be installed, see section \@ref(software-basics-r-packages), and the example dataset to be stored under name `dat`, see section \@ref(loading-data)):

```r
rosetta::freq(dat$currentEducation_cat);
```

To also order a barchart, use:

```r
rosetta::freq(dat$currentEducation_cat, plot=TRUE);
```

To order frequencies for multiple variables simultaneously, use:

```r
rosetta::frequencies(dat$currentEducation_cat,
                     dat$prevEducation_cat);
```

### descr and kableExtra packages

The [descr](https://cran.r-project.org/web/packages/descr/descr.pdf) package is used to run the "descriptive statistics" for the variable (in this case, a frequency table). By default, the freq command in the descr package will also create a basic bar graph. The [kableExtra](https://cran.r-project.org/web/packages/kableExtra/vignettes/awesome_table_in_html.html) package can be combined with many packages to create aesthetically pleasing tables.

<!---
```r
descr::freq(dat$currentEducation)            #view output

frq_1<-descr::freq(dat$currentEducation)     #create object of output
frq_1<-as.data.frame(frq_1)                  #change to a data frame (necessary for kable)
kable(frq_1, booktabs=T, digits=2) %>%       #create kable of data frame
  kable_styling()
```

This can also be done in one go and sans pipe:

--->

```r
kableExtra::kable_styling(
  knitr::kable(as.data.frame(descr::freq(dat$currentEducation_cat)),
               booktabs=T, digits=2));
```

In words:

1. From the **descr** package, use the frequencies command for the currentEducation variable from the dataset: `(descr::freq(dat$currentEducation_cat)`.
2. To make the aesthetically pleasing output, we are going to create a kable from the **kableExtra** package. Kables require dataframes, so we need to turn this frequency output into a dataframe: `as.data.frame()`.
3. Now, let's call the kable function from the knitr package: `knitr::kable()`.
4. And add some stylistic elements, such as (what does booktabs=T actually do?) `booktabs=T` and changing the number of decimal places to 2 digits `digits=2`.
5. Lastly, let's add kablestyling to make the kable aesthetically pleasing: `kableExtra::kable_styling()`.


## SPSS 

Use the following command (this requires the `dat` dataset to be the active dataset, see \@ref(software-basics-spss-active-dataset)):

```
FREQ VARIABLES=currentEducation_cat.
```

To also order a barchart, use:

```
FREQ VARIABLES=currentEducation_cat
  /BARCHART FREQ.
```

To order frequencies for multiple variables simultaneously, use:

```
FREQ VARIABLES=currentEducation_cat prevEducation_cat.
```


## Read more

If you would like more background on this topic, you can read more in these sources:

- Discovering Statistics with SPSS (closed access): 
- Learning Statistics with R [@navarro_learning_2018]: section XXXXXXXXXX, page XXXXXXXXXX (or [follow this link for the Bookdown version](https://bookdown.org/ekothe/navarro26/XXXXXXXXXX))

