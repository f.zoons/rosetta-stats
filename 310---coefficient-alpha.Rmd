---
output: html_document
editor_options: 
  chunk_output_type: console
---

# Coefficient Alpha {#coefficient-alpha}

## Intro

Coefficient Alpha [also known as Cronbach's Alpha, but Cronbach disliked that association; @cronbach_my_2004] is considered a measure of internal consistency and can, if a set of severe assumptions is met, estimate the reliability of a set of items.

### Example dataset

This example uses the Rosetta Stats example dataset "pp15" (see Chapter \@ref(datasets) for information about the datasets and Chapter \@ref(loading-data) for an explanation of how to load datasets).

```{r 310-coefficient-alpha-load-data}
data("pp15", package="rosetta");
```

### Variable(s)

```{r 310-coefficient-alpha-define-backtick}
backtick <- "`";
```

From this dataset, this example uses variables `r ufs::vecTxt(grep("highDose_AttGeneral", names(rosetta::pp15), value=TRUE), useQuote=backtick);`.

## Input: jamovi

In the "Analyses" tab, click the "Factor" button and from the menu that appears, select "Reliability Analysis" as shown in Figure \@ref(fig:310-coefficient-alpha-jamovi-1).

```{r 310-coefficient-alpha-jamovi-1, fig.cap='Opening the reliability analysis menu in jamovi'}
knitr::include_graphics(here::here("img","310-coefficient-alpha-jamovi-1.png"));
```

In the box at the left, select all variables you want to include in this analysis and move them to the box labelled "Items" using the button labelled with the rightward-pointing arrow as shown in Figure \@ref(fig:310-coefficient-alpha-jamovi-2).

```{r 310-coefficient-alpha-jamovi-2, fig.cap='Adding the items to analyse in jamovi'}
knitr::include_graphics(here::here("img","310-coefficient-alpha-jamovi-2.png"));
```

Because Coefficient Alpha (called Cronbach's Alpha in the jamovi interface) has already been checked (see the left-most column at the bottom labelled "Scale Statistics"), you will immediately see Coefficient Alpha for the selected items appear in the table in jamovi's Results pane on the right-hand side. You can now also order additional statistics, such as the value Coefficient Alpha would have if you were to omit each item, as shown in Figure \@ref(fig:310-coefficient-alpha-jamovi-3).

```{r 310-coefficient-alpha-jamovi-3, fig.cap='Ordering "Alpha if item dropped" in jamovi'}
knitr::include_graphics(here::here("img","310-coefficient-alpha-jamovi-3.png"));
```

## Input: R

### R: rosetta

```
rosetta::reliability(
  data = dat,
  items = c(
    "highDose_AttGeneral_good",
    "highDose_AttGeneral_prettig",
    "highDose_AttGeneral_slim",
    "highDose_AttGeneral_gezond",
    "highDose_AttGeneral_spannend"
  )
);
```

## Input: SPSS

To compute Coefficient Alpha in SPSS, use the following command:

```
RELIABILITY
  /VARIABLES =
    highDose_AttGeneral_good
    highDose_AttGeneral_prettig
    highDose_AttGeneral_slim
    highDose_AttGeneral_gezond
    highDose_AttGeneral_spannend
  /MODEL =
    ALPHA.
```

To order additional information, such as descriptive statistics, inter-item correlations, and other scale statistics, you can specify additional options:

```
RELIABILITY
  /VARIABLES =
    highDose_AttGeneral_good
    highDose_AttGeneral_prettig
    highDose_AttGeneral_slim
    highDose_AttGeneral_gezond
    highDose_AttGeneral_spannend
  /MODEL = ALPHA
  /STATISTICS = DESCRIPTIVE SCALE CORR
  /SUMMARY = TOTAL.
```

## Output: jamovi

```{r 310-coefficient-alpha-jamovi-4, fig.cap='The output of a reliability analysis where Coefficient Alpha is computed in jamovi', out.width='50%'}
knitr::include_graphics(here::here("img","310-coefficient-alpha-jamovi-4.png"));
```

## Output: R

### R: rosetta

```{r, results = "asis"}
rosetta::reliability(
  data = rosetta::pp15,
  items = c(
    "highDose_AttGeneral_good",
    "highDose_AttGeneral_prettig",
    "highDose_AttGeneral_slim",
    "highDose_AttGeneral_gezond",
    "highDose_AttGeneral_spannend"
  ),
  headingLevel = 4
);
```

## Output: SPSS

The SPSS output still has to be added.

