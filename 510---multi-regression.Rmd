---
output: html_document
editor_options: 
  chunk_output_type: console
---

# Multiple Regression Analysis {#multi-regression}

## Intro

Multivariate regression analysis can be useful to obtain a model to predict the dependent variable as a function of two or more predictor variables and estimate what proportion of the variance of that dependent variable can be understood using the predictor variables. The approach differs for continuous or categorical predictors, and both will be shown.

### Example dataset

This example uses the Rosetta Stats example dataset "pp15" (see Chapter \@ref(datasets) for information about the datasets and Chapter \@ref(loading-data) for an explanation of how to load datasets).

```{r 510-multi-regression-load-data}
data("pp15", package="rosetta");
```

### Variable(s)

```{r 510-multi-regression-define-backtick}
backtick <- "`";
```

From this dataset, this example uses variables `xtcUseDosePref` as dependent variable (the MDMA dose a participant prefers), `highDose_attitude`, `highDose_perceivedNorm`, and `highDose_pbc` as continuous predictors (the attitude, perceived norms, and perceived behavior control with respect to using a high dose of MDMA), and `hasJob_bi` as a categorical predictor (whether a participant has a job or not (e.g. is a student or unemployed)).

<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->

## Input: jamovi

In the "Analyses" tab, click the "Regression" button and from the menu that appears, select "Linear Regression" as shown in Figure \@ref(fig:410-uni-regression-jamovi-input-1).

```{r 510-multi-regression-jamovi-input-1, fig.cap='Opening the linear regression menu in jamovi'}
knitr::include_graphics(here::here("img","410-uni-regression-jamovi-input-1.png"))
```

In the box at the left, select the dependent variable and move it to the box labelled "Dependent variable" using the button labelled with the rightward-pointing arrow as shown in Figure \@ref(fig:410-uni-regression-jamovi-input-2).

```{r 510-multi-regression-jamovi-input-2, fig.cap='Selecting the dependent variable for the regression analysis in jamovi'}
knitr::include_graphics(here::here("img","410-uni-regression-jamovi-input-2.png"))
```

In the box at the left, select the continuous predictors and move them to the box labelled "Covariates" using the button labelled with the rightward-pointing arrow as shown in Figure \@ref(fig:510-multi-regression-jamovi-input-3).

```{r 510-multi-regression-jamovi-input-3, fig.cap='Selecting the continuous predictros for the regression analysis in jamovi'}
knitr::include_graphics(here::here("img","510-multi-regression-jamovi-input-1.png"))
```

Categorical predictors are moved to the box labelled "Factors" instead as shown in Figure \@ref(fig:510-multi-regression-jamovi-input-4).

```{r 510-multi-regression-jamovi-input-4, fig.cap='Selecting the categorical predictors for the regression analysis in jamovi'}
knitr::include_graphics(here::here("img","510-multi-regression-jamovi-input-2.png"))
```

The results will immediately be shown in the right-hand "Results" panel. You can scroll down to specify additional analyses, for example to order more details about the coefficients by opening the "Model Coefficients" section as shown in Figure \@ref(fig:510-multi-regression-jamovi-input-5).

```{r 510-multi-regression-jamovi-input-5, fig.cap='Opening the Model Coefficients section in jamovi'}
knitr::include_graphics(here::here("img","410-uni-regression-jamovi-input-4.png"))
```

For example, to request the confidence interval for the coefficient and the standardized (scaled) coefficients, check the corresponding check boxes as shown in Figure \@ref(fig:510-multi-regression-jamovi-input-6).

```{r 510-multi-regression-jamovi-input-6, fig.cap='Selecting the predictor for the regression analysis in jamovi'}
knitr::include_graphics(here::here("img","510-multi-regression-jamovi-input-3.png"))
```

## Input: R

### R: base R

In base R, the `lm` (linear model) function can be combined with the `summary` function to show the most important results. With a continuous predictor, the code is as follows. R automatically treats variables that are factors as categorical, and numeric vectors as continuous variables.

```r
result <-
  lm(
    xtcUseDosePref ~
      highDose_attitude +
      highDose_perceivedNorm +
      highDose_pbc +
      hasJob_bi,
    data=dat
  );
summary(
  result
);
```

### R: rosetta {#multi-regression-input-r-rosetta}

In the `rosetta` package, the `regr` function wraps base R's `lm` function to present output similar to that provided by other statistics programs in one command.

```r
rosetta::regr(
    xtcUseDosePref ~
      highDose_attitude +
      highDose_perceivedNorm +
      highDose_pbc +
      hasJob_bi,
  data=dat
);
```

Like `lm` in base R, the command is the same for a continuous predictor as it is for a categorical predictor. Additional output can be requested using arguments `collinearity=TRUE`, and when there are two predictors and an interaction term, `plot=TRUE`):

```r
rosetta::regr(
  xtcUseDosePref ~
    highDose_attitude +
    hasJob_bi +
    highDose_attitude:hasJob_bi,
  data=dat,
  collinearity=TRUE,
  plot=TRUE
);
```

## Input: SPSS

In SPSS, the `REGRESSION` command is used (don't forget the period at the end (`.`), the command terminator):

```
REGRESSION 
  /STATISTICS COEFF OUTS CI(95) R ANOVA 
  /DEPENDENT xtcUseDosePref 
  /METHOD=ENTER highDose_attitude highDose_perceivedNorm highDose_pbc hasJob_bi.
```

## Output: jamovi

```{r 510-multi-regression-jamovi-output-1, fig.cap='Multivariate regression analysis output in jamovi'}
knitr::include_graphics(here::here("img","510-multi-regression-jamovi-output-1.png"))
```

## Output: R

### R: base R

```{r, comment=""}
result <-
  lm(
    xtcUseDosePref ~
      highDose_attitude +
      highDose_perceivedNorm +
      highDose_pbc +
      hasJob_bi,
    data=pp15
  );
summary(
  result
);
```

### R: rosetta

```{r results="asis", fig.width=7, fig.height=7}
rosetta::regr(
  xtcUseDosePref ~
    highDose_attitude +
    hasJob_bi +
    highDose_attitude:hasJob_bi,
  data=pp15,
  collinearity=TRUE,
  plot=TRUE,
  headingLevel=4
);
```

## Output: SPSS

```{r 510-multiple-regression-spss-output, fig.cap='The output of the regression analysis in SPSS'}
knitr::include_graphics(here::here("img","510-multi-regression-spss-output.png"));
```

