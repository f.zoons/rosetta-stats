# Authors {#book-authors}

Because Rosetta Stats is not a traditional book, the list of authors is dynamic. Therefore, they are listed on this page. In addition, this page also lists the roles everybody has.

## Roles

To avoid diffusion of responsibility, the Rosetta Stats project has a set of roles that designate the primary responsibility in a number of domains. These are listed here.

### Overarching roles

These are a number of overarching roles relating to the Rosetta Stats project. These are the following.

- *Rosetta Stats Coordination Team*: These people are responsible for the general coordination of the Rosetta Stats project, enforcing the Code of Conduct, deciding to include new languages, et cetera. Currently, these are:
  - Danny Katz
  - Ron Pat-El
  - Gjalt-Jorn Peters
  - Peter Verboon
  - Melissa Wolf
  - Juul Coumans
- *Git, bookdown & CI*: These people are responsible for making sure the git repository, the Bookdown project, and GitLab's continuous integration functionality all keep doing their thing. Currently, these are:
  - Gjalt-Jorn Peters
- *Zotero group management*: These people are responsible for managing the admin memberships of the public Zotero group associated to this project (see https://www.zotero.org/groups/2419994/rosetta_stats). Currently, these are:
  - Gjalt-Jorn Peters

### Language-specific roles

This list lists who is responsible for each specific language.

- *jamovi*: Gjalt-Jorn Peters
- *R rosetta*: Gjalt-Jorn Peters
- *R tidyverse*: Melissa Wolf & Danny Katz
- *SAS*: ...
- *Stata*: ...
- *SPSS GUI*: Melissa Wolf
- *SPSS syntax*: Ron Pat-El

## People

These are the people who contribute to Rosetta Stats.

### Danny Katz

Danny is a PhD candidate in the school of Education at the University of California, Santa Barbara.  His advisor is [Andy Maul](https://education.ucsb.edu/research-faculty/bio?first=Andrew&last=Maul).

### Ron Pat-El

Ron Pat-El works at the Methodology and Statistics department of the faculty of Psychology at the Open University of the Netherlands. ...

### Gjalt-Jorn Ygram Peters

Gjalt-Jorn works at the Methodology and Statistics department of the faculty of Psychology at the Open University of the Netherlands. He enjoys leveraging R and other programming languages to address operational, methodological, and theoretical challenges in psychology. A list of the R packages he is involved in is available at https://gitlab.com/r-packages. Gjalt-Jorn's area of research of behavior change, and on those relatively rare occasions where he does substantive research, he applies this to nightlife-related risk behavior. His Twitter handle is [matherion](https://twitter.com/matherion).

### Peter Verboon

Peter Verboon works at the Methodology and Statistics department of the faculty of Psychology at the Open University of the Netherlands. He received his PhD long a ago at the Leiden University in the Netherlands, for research on robust nonlinear multivariate analysis. His present area of interest is in the analysis of longitudinal data, in particular obtained from single case designs (https://gitlab.com/Verboon/scda---single-case-design-analyses.git) or ecological momentary assessments (EMA) designs. He developed several R packages: https://github.com/PeterVerboon/lagnetw.git, https://github.com/PeterVerboon/cyclicpkg.git, and https://gitlab.com/r-packages/scda.git.

### Melissa Gordon Wolf

Melissa is a PhD student in the school of Education at the University of California, Santa Barbara.  She is interested in the design, validation, and analysis of self-report surveys.  Her advisor is [Andy Maul](https://education.ucsb.edu/research-faculty/bio?first=Andrew&last=Maul). To learn more, visit her website: https://www.melissagwolf.com/.
