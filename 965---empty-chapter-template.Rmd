---
output: html_document
editor_options: 
  chunk_output_type: console
---
# Empty Chapter Template {#empty-chapter-template}

## Intro

Enter a brief introduction here

### Example dataset

Here, explain which dataset people should open to copy the examples. For example:

This example uses the Rosetta Stats example dataset "pp15" (see Chapter \@ref(datasets) for information about the datasets and Chapter \@ref(loading-data) for an explanation of how to load datasets).

### Variable(s)

Here, explain which variable(s) people should open to copy the examples. For example:

From this dataset, this example uses variable `highDose_IntentionRAA_intention`.

<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->

## Input: jamovi

```{r fig.cap='A screenshot placeholder'}
knitr::include_graphics(here::here("img","rosetta-stats-screenshot-placeholder.png"));
```

<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->

## Input: R

In R, there are roughly three approaches. Many analyses can be done with base R without installing additional packages. The `rosetta` package accompanies this book and aims to provide output similar to jamovi and SPSS with simple commands. Finally, the tidyverse is a popular collection of packages that try to work together consistently but implement a different underlying logic than base R (and so, the `rosetta` package).

### R: base R

```{r fig.cap='A screenshot placeholder'}
knitr::include_graphics(here::here("img","rosetta-stats-screenshot-placeholder.png"));
```

### R: rosetta

```{r fig.cap='A screenshot placeholder'}
knitr::include_graphics(here::here("img","rosetta-stats-screenshot-placeholder.png"));
```

### R: tidyverse

```{r fig.cap='A screenshot placeholder'}
knitr::include_graphics(here::here("img","rosetta-stats-screenshot-placeholder.png"));
```

<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->

## Input: SPSS

For SPSS, there are two approaches: using the Graphical User Interface (GUI) or specify an analysis script, which in SPSS are called "syntax".

### SPSS: GUI

```{r fig.cap='A screenshot placeholder'}
knitr::include_graphics(here::here("img","rosetta-stats-screenshot-placeholder.png"));
```

### SPSS: Syntax

```{r fig.cap='A screenshot placeholder'}
knitr::include_graphics(here::here("img","rosetta-stats-screenshot-placeholder.png"));
```

<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->

## Output: jamovi

```{r fig.cap='A screenshot placeholder'}
knitr::include_graphics(here::here("img","rosetta-stats-screenshot-placeholder.png"));
```

<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->

## Output: R

```{r fig.cap='A screenshot placeholder'}
knitr::include_graphics(here::here("img","rosetta-stats-screenshot-placeholder.png"));
```

<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->

## Output: SPSS

```{r fig.cap='A screenshot placeholder'}
knitr::include_graphics(here::here("img","rosetta-stats-screenshot-placeholder.png"));
```

<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->
<!---------------------------------------------------------------------------->

## Read more

Here, you can list one or more sources with background reading, for example:

If you would like more background on this topic, you can read more in these sources:

- Learning Statistics with R [@navarro_learning_2018]: section XXXXXXXXXX, page XXXXXXXXXX (or [follow this link for the Bookdown version](https://bookdown.org/ekothe/navarro26/XXXXXXXXXX))
